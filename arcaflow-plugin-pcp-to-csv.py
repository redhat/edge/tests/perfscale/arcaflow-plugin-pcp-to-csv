#!/usr/bin/env python3

import sys
import yaml
from pathlib import Path
import re
import pprint

pp = pprint.PrettyPrinter(indent=4)

pcp_plugin_data = yaml.safe_load(sys.stdin)
pcp_data = pcp_plugin_data["output_data"]["pcp_output"]


# This function flattens a YAML object from the output of the
# arcaflow-plugin-pcp tool. This is configured to work with a
# known use case and pmlogger metrics, and it may not work
# correctly with different metrics output.
def flatten_object(object, metrics_list=[], flat=""):
    for k, v in object.items():
        if k in ("@interval"):
            continue
        elif k in ("@timestamp"):
            metrics_list.append(("timestamp", v))
            continue
        elif k in ("@unit"):
            flat_append = v
            continue
        elif k in ("value"):
            metrics_list.append((f"{flat}.{flat_append}", v))
            continue
        elif isinstance(v, dict):
            flat += f".{k}"
            value = v
            if flatten_object(v, metrics_list, flat):
                flat = re.sub(r'\.(?:.(?!\.))+$', '', flat)
                continue
        elif isinstance(v, list):
            for l in v:
                if isinstance(l, dict):
                    for m, n in l.items():
                        if m in "name":
                            flat_append = n
                        elif m in "value":
                            value = n
                metrics_list.append((f"{flat}.{flat_append}", value))
    return(metrics_list)
            

# Iterate the arcaflow-plugin-pcp YAML output through
# the flatten_object function
metrics_list = []
for i in pcp_data:
    metrics_list = flatten_object(i)

metrics_dict = {}
rows = 0
# Pre-populate metrics dictionary keys and
# get the row count from the timestamps
for metric in metrics_list:
    if metric[0] not in metrics_dict.keys():
        metrics_dict[metric[0]] = []
    if metric[0] in ("timestamp"):
        rows += 1    

# Populate the metrics into the dictionary
for metric in metrics_list:
    metrics_dict[metric[0]].append(metric[1])

# Backfill any metric data points missing from the first timestamp
for existing in metrics_dict.keys():
    if len(metrics_dict[existing]) < rows:
        # This is a hack that works with our known use case
        # but isn't really a proper solution
        metrics_dict[existing].insert(0, None)



csv_list = []
newlist = []
# Prepare the CSV as a list of lists
# The first row is the dictionary keys as headers
for key in metrics_dict.keys():
    newlist.append(key)
# Append the header list to the parent list
csv_list.append(newlist)
# Iterate through the metrics dictionary to populate the data rows
for count, timestamp in enumerate(metrics_dict["timestamp"]):
    newlist = []
    for item in metrics_dict.keys():
        newlist.append(metrics_dict[item][count])
    csv_list.append(newlist)

# Print the CSV
for line in csv_list:
    print(str(line).strip('[]'))